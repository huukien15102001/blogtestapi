﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BlogTestApi.Data;
using BlogTestApi.Models;
using BlogTestApi.Repositories;
namespace BlogTestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogItemsController : ControllerBase
    {
        private readonly IBlogRepository _blogRepository;

        public BlogItemsController (IBlogRepository blogRepository)
        {
            _blogRepository = blogRepository;
        }
        
        // GET: api/BlogItems
        [HttpGet]
        public async Task<IEnumerable<BlogItem>> GetBlogItem()
        {
            return await _blogRepository.GetList();
        }

        // GET: api/BlogItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BlogItem>> GetBlogItem(long id)
        {
            return await _blogRepository.Get(id);
        }

        // PUT: api/BlogItems/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBlogItem(long id, BlogItem blogItem)
        {
            if (id != blogItem.Id)
            {
                return BadRequest();
            }

            await _blogRepository.Update(blogItem);

            return NoContent();
        }

        // POST: api/BlogItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<BlogItem>> PostBlogItem(BlogItem blogItem)
        {
            var newBlog = await _blogRepository.Post(blogItem);

            return CreatedAtAction(nameof(GetBlogItem),
                new { id = newBlog.Id },
                newBlog);
        }

        // DELETE: api/BlogItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBlogItem(long id)
        {
            var blogItem = await _blogRepository.Get(id);
            if (blogItem == null)
            {
                return NotFound();
            }
            await _blogRepository.Delete(blogItem.Id);
            return NoContent();
        }

    }
}
