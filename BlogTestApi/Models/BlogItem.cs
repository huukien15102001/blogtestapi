﻿using System;

namespace BlogTestApi.Models
{
    public class BlogItem
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Des { get; set; }
        public string Detail { get; set; }
        public string Position { get; set; }
        public bool IsPublic { get; set; }
        public int CategoryId { get; set; }
        public DateTime DatePublish { get; set; }
    }
}
