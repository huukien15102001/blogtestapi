﻿using BlogTestApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Mvc;

namespace BlogTestApi.Repositories
{
    public interface IBlogRepository
    {
        Task<IEnumerable<BlogItem>> GetList();
        Task<BlogItem> Get(long id);
        Task<BlogItem> Post(BlogItem blog);
        Task Update(BlogItem blog);
        Task Delete(long id);
        
    }
}
