﻿using BlogTestApi.Data;
using BlogTestApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace BlogTestApi.Repositories
{
    public class BlogRepository : IBlogRepository
    {
        private readonly BlogTestApiContext _context;

        //constructor
        public BlogRepository(BlogTestApiContext context)
        {
            _context = context;
        }


        // DELETE api/<BlogController>/5
        public async Task Delete(long id)
        {
           var blogToDelete = await _context.BlogItems.FindAsync(id);
            _context.Remove(blogToDelete);
            await _context.SaveChangesAsync();
        }

        // GET api/<BlogController>/5
        public async Task<BlogItem> Get(long id)
        {
            return await _context.BlogItems.FindAsync(id);
        }

        // GET: api/<BlogController>
        public async Task<IEnumerable<BlogItem>> GetList()
        {
            return await _context.BlogItems.ToListAsync();
        }

        // POST api/<BlogController>
        public async Task<BlogItem> Post(BlogItem blog)
        {
            _context.BlogItems.Add(blog);
            await _context.SaveChangesAsync();
            return blog;
        }

        // PUT api/<BlogController>/5
        public async Task Update(BlogItem blog)
        {
            _context.Entry(blog).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
