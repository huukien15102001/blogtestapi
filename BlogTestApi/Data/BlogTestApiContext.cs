﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BlogTestApi.Models;

namespace BlogTestApi.Data
{
    public class BlogTestApiContext : DbContext
    {
        public BlogTestApiContext (DbContextOptions<BlogTestApiContext> options)
            : base(options)
        {
        }

        public DbSet<BlogTestApi.Models.BlogItem> BlogItems { get; set; }
    }
}
